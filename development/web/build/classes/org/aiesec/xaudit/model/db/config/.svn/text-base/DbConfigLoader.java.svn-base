/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.yucatan.cj.sindesy.model.db.config;

import gov.yucatan.cj.sindesy.model.db.exceptions.MoreThanOneDbServerActiveException;
import gov.yucatan.cj.sindesy.model.db.exceptions.NoneActiveServerException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Romms
 */
public class DbConfigLoader {
    private static final String SERVER="server";
    private static final String IS_ACTIVE="isActive";
    private static final String DB_SERVER_NAME="dbServerName";
    private static final String DB_POOL_SIZE="dbPoolSize";
    private static final String DB_STREAM="dbStream";
    private static final String DB_HOST="dbHost";
    private static final String DB_PORT="dbPort";
    private static final String DB_USER="dbUser";
    private static final String DB_PASS="dbPass";
    private static final String DB_SCHEMA="dbSchema";
    private static final String DB_DRIVER="dbDriver";
    private static final String ACTIVE="1";

    private DbConfigData dbConfiguration;
    private Document document;

    public DbConfigLoader(String dbConfigFileName) throws FileNotFoundException{
        dbConfiguration=new DbConfigData();
        parseXmlFile(dbConfigFileName);
    }

    private void parseXmlFile(String fileName) throws FileNotFoundException{
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(fileName);
        } catch (IOException ex) {
            Logger.getLogger(DbConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(DbConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DbConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void parseDbConfigFile() throws MoreThanOneDbServerActiveException, NoneActiveServerException{
        Element documentElement= document.getDocumentElement();
        NodeList nodeList = documentElement.getElementsByTagName(SERVER);
        if(nodeList != null && nodeList.getLength() > 0){
            boolean isOneServerActive = false;
            for(int node=0;node<nodeList.getLength();node++){
                Element element= (Element)nodeList.item(node);
                if(getElementValue(element, IS_ACTIVE).compareTo(ACTIVE)==0 && isOneServerActive==false){
                    isOneServerActive=true;
                    this.dbConfiguration = retreiveDbConfiguration(element);
                    
                }else if(getElementValue(element, IS_ACTIVE).compareTo(ACTIVE)==0 && isOneServerActive==true){
                    throw new MoreThanOneDbServerActiveException();
                }
            }
            if(!isOneServerActive){
                throw new NoneActiveServerException();
            }
        }
    }

    private String getElementValue(Element element, String tagName){
        String elementValue=null;
        NodeList nodeList = element.getElementsByTagName(tagName);
        if(nodeList!= null && nodeList.getLength() > 0){
            Element currentElement = (Element) nodeList.item(0);
            if(currentElement.getFirstChild()!=null){
                elementValue = currentElement.getFirstChild().getNodeValue();
            }else{
                elementValue="";
            }
        }
        //System.out.println("Tag:"+tagName +"=" + elementValue);
        return elementValue;
    }

    private DbConfigData retreiveDbConfiguration(Element element){
        if(getElementValue(element, IS_ACTIVE).compareToIgnoreCase(ACTIVE)==0){
            dbConfiguration.setDbServerName(getElementValue(element, DB_SERVER_NAME));
            dbConfiguration.setDbPoolSize(Integer.parseInt(getElementValue(element, DB_POOL_SIZE)));
            dbConfiguration.setDbStream(getElementValue(element, DB_STREAM));
            dbConfiguration.setDbHost(getElementValue(element, DB_HOST));
            dbConfiguration.setDbPort(getElementValue(element, DB_PORT));
            dbConfiguration.setDbUser(getElementValue(element, DB_USER));
            dbConfiguration.setDbPass(getElementValue(element, DB_PASS));
            dbConfiguration.setDbSchema(getElementValue(element, DB_SCHEMA));
            dbConfiguration.setDbDriver(getElementValue(element, DB_DRIVER));
            return dbConfiguration;
        }
        return null;
    }

    public void loadDbConfiguration() throws MoreThanOneDbServerActiveException, NoneActiveServerException{
        this.parseDbConfigFile();
    }

    public DbConfigData getDbConfiguration(){
        return this.dbConfiguration;
    }
}
