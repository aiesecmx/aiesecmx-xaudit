<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>AIESEC Mexico | Contacto</title>
    <meta charset="utf-8">
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Your name">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-1.7.2.js"></script>

    <script src="js/cufon-yui.js"></script>

	<style type="text/css">cufon{text-indent:0!important;}@media screen,projection{cufon{display:inline!important;display:inline-block!important;position:relative!important;vertical-align:middle!important;font-size:1px!important;line-height:1px!important;}cufon cufontext{display:-moz-inline-box!important;display:inline-block!important;width:0!important;height:0!important;overflow:hidden!important;text-indent:-10000in!important;}cufon canvas{position:relative!important;}}@media print{cufon{padding:0!important;}cufon canvas{display:none!important;}}</style>
    

    <script src="js/cufon-replace.js"></script>

    <script src="js/script.js"></script>
	<script type="text/javascript" src="js/forms.js"></script>
	
	<link rel="stylesheet" href="js/jquery/development-bundle/themes/redmond/jquery.ui.all.css">
	<script src="js/jquery/development-bundle/external/jquery.bgiframe-2.1.2.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.mouse.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.draggable.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.position.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.resizable.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.ui.dialog.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.effects.core.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.effects.blind.js"></script>
	<script src="js/jquery/development-bundle/ui/jquery.effects.explode.js"></script>
	
	<script src="js/formValidator.js"></script>
    
	<!--[if lt IE 7]>
  		<div class='aligncenter'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg"border="0"></a></div>  
 	<![endif]-->
    <!--[if lt IE 9]>
   		<script src="js/html5.js"></script>
  		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
	
	<script type="text/javascript">
		
		function sendMail(){
			if(!isContactFormValid()) return;
			
			var dataString = 'name='+ $('#name').val() + '&email=' + $('#email').val() + '&message=' + $('#message').val();
			
			$.ajax({  
				  type: "POST",  
				  url: "php/sendMail.php",  
				  data: dataString,  
				  success: function() {  
						$('#success').show();
						$('#reset').click();
				  }  
				});
		}
	
		
	    $(document).ready(function() {
	       	$.get("privacyPolicy.html",
	       			function(data){
	       				$("#privacyPolicy").html(data);
			});
		});
	    
		function showPrivacyPolicy(){
			$('html, body').animate({ scrollTop: 0 }, 'slow');
			$( "#privacyPolicy" ).dialog( "open" );
			return false;
		}
		
		$.fx.speeds._default = 250;
		$(function() {
			$("#privacyPolicy" ).dialog({
				autoOpen: false,
				modal: true,
				show: "blind",
				hide: "explode",
				resizable: false,
				width: "700px"
			});
		});
	
	    twttr.anywhere(function (T) {
	       T.bind("authComplete", function (e, user) {
	          // triggered when auth completed successfully
	        });
	
		   T.bind("signOut", function (e) {
	          // triggered when user logs out
	        });
	     });
	</script>
	
	<script type="text/javascript">
	    twttr.anywhere(function (T) {
		   T("#tweet").hovercards({ expanded: true });
	     });
	</script>
	
	<script type="text/javascript">
	  	
		function login(){
			alert('calling login');
			var url="http://localhost:8080/aiesecmx-xaudit/login.action?userId=admin&password=admin";
			$.ajax({  
				  type: "POST",  
				  url: url,    
				  success: function(jsonData) {  	
					    //alert(jsonData);
					  window.location.replace("welcomeAdmin.jsp");
				  }
				});
		}
		
		function retriveUsers(){
			$.ajax({  
				  type: "POST",  
				  url: "showUsersManagement.action",    
				  success: function(jsonData) {  	
					    var obj = JSON.parse(jsonData);
					    $("#myTable").html(JSON.stringify(obj));
				  }
				});
		}
	
	</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!--==============================header=================================-->
<header>
	<span>
		<div class="fleft"><h1 class="logo"><a href="home.html">AIESEC M&eacute;xico</a></h1></div>
	</span>
	
	<div align="right">
		New to Xaudit? <a class="buttonRegister" href="register.html">Register</a>
	</div>
	
	<div class="clear"></div>

</header>
<!--==============================content================================-->
<section id="content" class="padcontent2">
	<div class="container_12">
		<div class="wrapper">
			<div class="grid_7">
				<h1 class="borderbottom indent4">Xaudit System</h1>
			</div>
			<div class="grid_5">
				<h1>Sign in </h1>
				<form id="contact-form">

                    <div id="success" class="success" style="display:none"> Registro completo! <br><strong>Espera noticias nuestras.</strong></div>

                    <fieldset>
                        <label class="email">

                            <input type="text" value="Username" id="user">

                            <span id="emailError" class="error">*Email no v&aacute;lido.</span>

                            <span id="emailEmpty" class="empty">*El Email es obligatorio.</span>

                            <span class="clear"></span>

                        </label>
                        <label class="name">

                            <input id="password" type="text" value="Password">

                            <span id="nameError" class="error">*Nombre no v&aacute;lido.</span>

                            <span id="nameEmpty" class="empty">*El nombre es obligatorio.</span>

                            <span class="clear"></span>

                        </label>

                        <div class="buttons">
							<a class="button" href="javascript:login();" >Sign in</a>
							<a>Can't access you account?</a>
						</div>

                    </fieldset>

                </form>

			</div>
		</div>
	</div>
	
</section>
<!--==============================footer=================================-->
	<footer>
		<div class="wrapper">
				<div class="fleft">AIESEC &copy; 2012 <a href="javascript:showPrivacyPolicy()">Privacy Policy</a> <!-- {%FOOTER_LINK} --></div>
		</div>
	</footer>
	
	<script>Cufon.now()</script>
	
	<div id="privacyPolicy"></div>
</body>
</html>
