$(function(){
	tabs.init();
});	
tabs = {
	init : function(){
		$('.tabs').each(function(){
			$(this).find('.tab-content').hide();
			$($(this).find('ul.nav .selected a').attr('href')).fadeIn(300);
			$(this).find('ul.nav a').click(function(){
				$(this).parents('.tabs').find('.tab-content').hide();
				$($(this).attr('href')).fadeIn(300);
				$(this).parent().addClass('selected').siblings().removeClass('selected').filter('.nav2 li').find('a').stop().animate({backgroundColor:'#bdbdbd',backgroundPosition:'20px 50%',paddingLeft:'36px'},'fast','easeOutQuad');
				return false;
			});
		});
	}
}