$(document).ready(function(){
	$("#send").click(function() {
		if(!dataIsValid())
			return;
		
		var dataString = 'name='+ $('#name').val() + '&lastName=' + $('#lastName').val() + '&email=' + $('#email').val()+ '&bday=' + $('#bday').val()+ '&state=' + $('#state').val()+'&university=' + $('#university').val()+ '&studyArea=' + $('#studyArea').val()+ '&graduation=' + $('#graduation').val()+'&why=' + $('#why').val()+ '&where=' + $('#where').val()+ '&languages=' + languages;
		
		$.ajax({  
			  type: "POST",  
			  url: "php/sendMailCommunity.php",  
			  data: dataString,  
			  success: function() {  	
					$('#success').show();
					$('#reset').click();
			  }  
		});
		return false;
	});
});