		var formIsValid=true;
		//Validation for the name
		function validateName(){
			var name=$('#name').val();
			if(name.length<3){
				$("#nameError").show();
				formIsValid=false;
			}else if(name=="Nombre"){
				$("#nameEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the last name
		function validateLastName(){
			var name=$('#lastName').val();
			if(name.length<3){
				$("#lastNameError").show();
				formIsValid=false;
			}else if(name=="Apellido"){
				$("#lastNameEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the Email
		function validateEmail(){
			var email=$("#email").val();
		    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    
		  	if(email=="E-mail"){
		  		$("#emailEmpty").show();
		  		formIsValid=false;
		  	}else if(!re.test(email)){
		  		$("#emailError").show();
		  		formIsValid=false;
		  	}
		}
		
		//Validation for the state
		function validateState(){
			debugger;
			var state=$('#state').val();
			if(state=="empty"){
				$("#stateEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for university
		function validateUniversity(){
			var university=$('#university').val();
			if(university=="Universidad" || university.length==0){
				$("#universityError").show();
				formIsValid=false;
			}else if(university.length<3){
				$("#universityEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for why
		function validateWhy(){
			var why=$('#why').val();
			if(why.length==0 || why=="Porque te quieres ir a un intercambio de AIESEC?"){
				$("#whyEmpty").show();
				formIsValid=false;
			}else if(why.length<3){
				$("#whyError").show();
				formIsValid=false;
			}
		}
		
		//Validation for where
		function validateWhere(){
			var where=$('#where').val();
			if(where.length==0 || where=="En donde escuchaste hablar de AIESEC?"){
				$("#whereEmpty").show();
				formIsValid=false;
			}else if(where.length<3){
				$("#whereError").show();
				formIsValid=false;
			}
		}
		
		//Validation for message
		function validateMessage(){
			var message=$('#message').val();
			if(message.length==0 || message=="Mensaje"){
				$("#messageEmpty").show();
				formIsValid=false;
			}else if(message.length<3){
				$("#messageError").show();
				formIsValid=false;
			}
		}
		
		//Validation for the study Area
		function validateStudyArea(){
			var studyArea=$('#studyArea').val();
			if(studyArea=="empty"){
				$("#studyAreaEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the company
		function validateCompany(){
			var company=$('#company').val();
			if(company.length<3){
				$("#companyError").show();
				formIsValid=false;
			}else if(company=="Empresa"){
				$("#companyEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the phone
		function validatePhone(){
			var phone=$('#phone').val();
			if(phone.length<6){
				$("#phoneError").show();
				formIsValid=false;
			}else if(phone=="Telefono con lada"){
				$("#phoneEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the message
		function validateMessage(){
			var message=$('#message').val();
			if(message.length<15){
				$("#messageError").show();
				formIsValid=false;
			}else if(message=="Mensaje"){
				$("#messageEmpty").show();
				formIsValid=false;
			}
		}
		
		//Validation for the job
		function validateJob(){
			var job=$('#job').val();
			if(job.length<3){
				$("#jobError").show();
				formIsValid=false;
			}else if(job=="Puesto"){
				$("#jobEmpty").show();
				formIsValid=false;
			}
		}
		
		function dataIsValid(){
			validateName();
			validateLastName();
			validateEmail();
			validateState();
			validateUniversity();
			validateStudyArea();
			validateWhy();
			validateWhere();
			
			if(formIsValid){
				return true;
			}else{
				formIsValid=true;
				return false;
			}
		}
		
		function isContactFormValid(){
			validateName();
			validateEmail();
		}
		
		function partnerDataIsValid(){
			validateName();
			validateCompany();
			validateJob();
			validateEmail();
			validatePhone();
			validateMessage();
			
			if(formIsValid){
				return true;
			}else{
				formIsValid=true;
				return false;
			}
		}