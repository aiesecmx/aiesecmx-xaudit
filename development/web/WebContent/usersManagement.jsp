<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"> 
	<thead> 
		<tr>  
			<th>Nombre</th>  
			<th colspan="3">Acciones</th> 
		</tr> 
	</thead> 
	<tbody> 
		<s:iterator value="listOfUsers">
		<tr> 
			<td>
				<s:property value="name"/>
			</td> 
			<td>
				UsrInfo
			</td>
			<td>
				EditUsr
			</td>
			<td> 
				Delete
			</td>
		</tr> 
		</s:iterator>
	</tbody> 
</table> 

</body>
</html>