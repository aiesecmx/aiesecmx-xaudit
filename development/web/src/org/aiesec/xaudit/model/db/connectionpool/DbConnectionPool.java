/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.connectionpool;

import org.aiesec.xaudit.model.db.config.DbConfigData;
import org.aiesec.xaudit.model.db.config.DbConfigLoader;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Romms
 */
public class DbConnectionPool implements Observer{
     private int dbConnectionsPoolSize;
     private Stack <DbConnection> availableDbConnections;
     private Stack <DbConnection> noActualBusyConnections;
     private Stack <DbConnection> actualBusyConnections;
     private DbConfigData currentDbConfigData;

     private DbConfigLoader dbConfigLoader;
     private boolean isAvailable;
     //private FileWatcher watcher;
     //private ConfigAdmin configAdmin;


     /**
      *
      * @throws SQLException
      * @throws IOException
      * @throws Exception
      */
     protected DbConnectionPool(DbConfigData currentDbConfigData) throws SQLException, IOException, Exception{
            this.currentDbConfigData=currentDbConfigData;
            dbConnectionsPoolSize = currentDbConfigData.getDbPoolSize();
            availableDbConnections = new Stack<DbConnection>();
            noActualBusyConnections = new Stack<DbConnection>();
            actualBusyConnections = new Stack<DbConnection>();
            this.createDbConnections();
            this.openDBConnections();
            this.isAvailable = true;
            //configWatcher();
    }

    /**
    * Crea las conexiones a la base de datos.
    * @throws SQLException
     * @throws Exception
    */
     private void createDbConnections() throws SQLException, Exception{
        for(int i=0; i<dbConnectionsPoolSize; i++){
            availableDbConnections.push(DbConnection.loadDbConnection());
        }
     }

     /**
      * Abre las conexiones a la base de datos.
      * @throws SQLException
      * @throws Exception
      */
     private void openDBConnections()throws SQLException, ClassNotFoundException{
        for(int i=0; i<availableDbConnections.size(); i++){
            availableDbConnections.elementAt(i).openConnection(this.currentDbConfigData);
        }
     }
     /**
      * Cierra las conexiones a la base de datos.
      * @throws Exception
      */

     private void closeDbConnections() throws Exception{
        for(int i=0; i<availableDbConnections.size(); i++){
            availableDbConnections.elementAt(i).closeConnection();
        }
     }

     /**
      * Proporciona una conexion ya establecida a la base de datos.
      * @return DbConnection
      */
     protected DbConnection getDbConnection() throws Exception{
            if(availableDbConnections.size() != 0){
                actualBusyConnections.push(availableDbConnections.pop());
                return  actualBusyConnections.lastElement();
            }else{
                this.reSizePool(this.dbConnectionsPoolSize+5);
                actualBusyConnections.push(availableDbConnections.pop());
                return  actualBusyConnections.lastElement();
            }
     }

     /**
      * Reajusta el tamaño del pool de conexiones disponibles.
      * Cuando se requieran mas o menos de los que existen.
      * @param size
      * @throws Exception
      */
     protected void reSizePool(int size) throws Exception{
        int numAvailableConnections = size-(actualBusyConnections.size()+noActualBusyConnections.size());
        if(numAvailableConnections>0){
            if(numAvailableConnections>availableDbConnections.size()){
                for(int i = 0; i<(numAvailableConnections-availableDbConnections.size()); i++){
                    availableDbConnections.push(DbConnection.loadDbConnection());
                    availableDbConnections.lastElement().openConnection(this.currentDbConfigData);
                }
            }
            else{
                for(int i = 0; i<(numAvailableConnections-availableDbConnections.size()); i++){
                    availableDbConnections.pop();
                }
            }

        }

     }


     /**
      * Regresa una conexion que ya fue utilizada a la pila de las conexiones
      * disponibles.
      * @param connection
      * @throws Exception
      */
     protected void returnDbConnection(DbConnection connection) throws Exception{
        for(int i = 0; i<actualBusyConnections.size(); i++){
            if(actualBusyConnections.elementAt(i).equals(connection)){
                availableDbConnections.push(actualBusyConnections.get(i));
                actualBusyConnections.remove(i);
            }
        }

        for(int j = 0; j<noActualBusyConnections.size(); j++){
            if(noActualBusyConnections.elementAt(j).equals(connection)){
                noActualBusyConnections.elementAt(j).closeConnection();
                noActualBusyConnections.elementAt(j).openConnection(this.currentDbConfigData);
                availableDbConnections.push(noActualBusyConnections.get(j));
                noActualBusyConnections.remove(j);
            }
        }

     }

     /**
      * Mueve todas las conexiones de la pila de conexiones actualizadas a
      * las no actualizadas.
      */
     private void moveDbConnection(){
        for(int i = 0; i<actualBusyConnections.size(); i++){
            noActualBusyConnections.push(actualBusyConnections.pop());
        }
    }

    /**
     * Cierra las conexiones de la pila de conexiones disponibles
     * y las vuelve a abrir.
     * @throws Exception
     */
     private void refreshDbConnections() throws Exception{
         this.closeDbConnections();
         this.openDBConnections();
     }

     /**
      * Carga la configuracion para la conexion a la base de datos.
      */
     private void loadDbConfig(){
        try {
            this.dbConfigLoader.loadDbConfiguration();
            this.currentDbConfigData = dbConfigLoader.getDbConfiguration();
        } catch (MoreThanOneDbServerActiveException ex) {
            Logger.getLogger(DbConnectionPool.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoneActiveServerException ex) {
            Logger.getLogger(DbConnectionPool.class.getName()).log(Level.SEVERE, null, ex);
        }
     }


     /**
      *
      * @return
      */
     protected boolean isAvailable(){
        return this.isAvailable;
    }

    /**
     *
     */
    protected void changeAvailability(boolean poolState){
        isAvailable= poolState;
    }

    /**
     * Cuando el archivo de configuracion ha sido modificado hace lo siguiente.
     *  -Carga la nueva configuracion.
     *  -Mueve todas las conexiones de la pila de conexiones actualizadas a la
     *   pila de conexiones no actualizadas,
     *  -Cierra las conexiones de la pila de conexiones disponibles y las vuelve
     *   a abrir con la nueva configuracion.
     * @param o
     * @param arg
     */

    protected DbConfigData getCurrentDbConfigData() {
        return currentDbConfigData;
    }

    protected void setCurrentDbConfigData(DbConfigData currentDbConfigData) {
        this.currentDbConfigData = currentDbConfigData;
    }
    
    public void update(Observable o, Object arg) {
        try {
            this.loadDbConfig();
            this.moveDbConnection();
            this.refreshDbConnections();
        } catch (Exception ex) {
            Logger.getLogger(DbConnectionPool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
