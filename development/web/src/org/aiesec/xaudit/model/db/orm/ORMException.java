/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.orm;

/**
 *
 * @author Romms
 */
public class ORMException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ORMException(Exception exception){
     super(exception);
    }

    public ORMException(String message, Exception exception){
        super(exception);
    }

    public String getExceptionMessage(){
        return this.getMessage();
    }
    
}
