/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.connectionpool;

import org.aiesec.xaudit.model.db.config.DbConfigLoader;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Romms
 */
public class DbConnectionManager {
    private DbConnectionPool dbConnectionPool;
    private static DbConnectionManager instance=null;
    DbConfigLoader dbConfigLoader;
    private static String dbConfigFileName="../config/DbConfig.xml";

     public static DbConnectionManager getInstance() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        if (instance == null) {
             instance = new DbConnectionManager();
        }
        return instance;
    }


    private DbConnectionManager() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception/* throws SQLException, IOException, Exception*/{
        try { 
            dbConfigLoader= new DbConfigLoader(dbConfigFileName);
            dbConfigLoader.loadDbConfiguration();
            dbConnectionPool = new DbConnectionPool(dbConfigLoader.getDbConfiguration());
            //pool.configWatcher(); hay que añadir esta para que cuando se cambie el archivo se refleje en el pool
        }catch (IOException ex) {
            Logger.getLogger(DbConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DbConnection getConnection(){
        if(this.dbConnectionPool.isAvailable()){
            try {
                return dbConnectionPool.getDbConnection();
            } catch (Exception ex) {
                Logger.getLogger(DbConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public void closeDbConnection(DbConnection connnection) throws Exception{
        dbConnectionPool.returnDbConnection(connnection);
    }


    public void changePoolAvailability(boolean poolState){
        this.dbConnectionPool.changeAvailability(poolState);
    }

    public static String getDbConfigFileName() {
        return dbConfigFileName;
    }

    public static void setDbConfigFileName(String dbConfigFileName) throws FileNotFoundException {
        if(!new File(dbConfigFileName).exists()){
            throw new FileNotFoundException();
        }
        DbConnectionManager.dbConfigFileName = dbConfigFileName;
    }
}
