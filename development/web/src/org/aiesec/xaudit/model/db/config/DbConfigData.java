/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.config;

/**
 *
 * @author Romms
 */
public class DbConfigData {
    private String dbServerName;
    private int dbPoolSize;
    private String dbStream;
    private String dbHost;
    private String dbPort;
    private String dbUser;
    private String dbPass;
    private String dbSchema;
    private String dbDriver;

    public DbConfigData(){
    }

    public String getURL(){
        String url = this.getDbStream()+this.getDbHost()+":"+this.getDbPort()+"/"+this.getDbSchema();
        return url;
    }

    @Override
    public String toString(){
        return this.getDbServerName()+" "+this.getURL()+"@"+this.getDbUser()+"@"+this.getDbPass()+"@"+this.getDbDriver();
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }

    public String getDbHost() {
        return dbHost;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public String getDbPass() {
        return dbPass;
    }

    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }

    public int getDbPoolSize() {
        return dbPoolSize;
    }

    public void setDbPoolSize(int dbPoolSize) {
        this.dbPoolSize = dbPoolSize;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public String getDbServerName() {
        return dbServerName;
    }

    public void setDbServerName(String dbServerName) {
        this.dbServerName = dbServerName;
    }

    public String getDbStream() {
        return dbStream;
    }

    public void setDbStream(String dbStream) {
        this.dbStream = dbStream;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

}
