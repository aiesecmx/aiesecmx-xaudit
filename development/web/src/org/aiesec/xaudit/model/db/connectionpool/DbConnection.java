/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.connectionpool;



import org.aiesec.xaudit.model.db.config.DbConfigData;
//import org.aiesec.xaudit.model.db.config.DbConfigLoader;
import org.aiesec.xaudit.model.db.exceptions.ConnectionNotFoundException;
import org.aiesec.xaudit.model.db.exceptions.DataBaseException;
import org.aiesec.xaudit.model.db.exceptions.InvalidStatementException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author romms
 */
public class DbConnection {
    
    //private DbConfigLoader dbConfigLoader;
    private Connection connection;
    private static DbConnection instance=null;

    private DbConnection(){
    }

    protected static DbConnection loadDbConnection(){
        if(instance==null){
            instance = new DbConnection();
        }
        return instance;
    }

    protected void openConnection(DbConfigData dbConfigData) throws SQLException, ClassNotFoundException{
            Class.forName(dbConfigData.getDbDriver());
            connection = DriverManager.getConnection(dbConfigData.getURL(), dbConfigData.getDbUser(), dbConfigData.getDbPass());
            System.out.println(dbConfigData.getURL()+","+dbConfigData.getDbUser()+","+dbConfigData.getDbPass());
    }

    protected void closeConnection() throws ConnectionNotFoundException{
        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException exception) {
            throw new ConnectionNotFoundException(exception);
        }
    }

    public ResultSet executeQuery(String sqlSentence) throws DataBaseException, ConnectionNotFoundException, InvalidStatementException{
        try {
        	
            Statement sqlStatementExecuter=connection.createStatement();
            ResultSet resultSet = sqlStatementExecuter.executeQuery(sqlSentence);
            return resultSet;
        }catch(NullPointerException exception){
            throw new ConnectionNotFoundException(exception);
        }catch (SQLException exception) {
            throw new  InvalidStatementException(exception);
        }
    }

    public void executeUpdate(String sqlSentence) throws DataBaseException, ConnectionNotFoundException, InvalidStatementException{
        try {
            Statement sqlStatementExecuter=connection.createStatement();
            sqlStatementExecuter.executeUpdate(sqlSentence);
        }catch(NullPointerException exception){
           throw new ConnectionNotFoundException(exception);
        } catch (SQLException exception) {
           throw new  InvalidStatementException(exception);
        }
    }

    public Connection getConnectio(){
        return this.connection;
    }

}
