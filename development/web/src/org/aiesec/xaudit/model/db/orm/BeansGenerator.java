
package org.aiesec.xaudit.model.db.orm;



import java.lang.reflect.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Romms
 */
public class BeansGenerator {

    private final String SET_PREFIX="set";

    public Class<?> getBeanKind() {
        return beanKind;
    }

    public void setBeanKind(Class<?> beanKind) {
        this.beanKind = beanKind;
    }

    public ResultSet getQueryResult() {
        return queryResult;
    }

    public void setQueryResult(ResultSet queryResult) {
        this.queryResult = queryResult;
    }

    private Class<?> beanKind;
    private ResultSet queryResult;

    public Object generateBean(ResultSet queryResult, Class<?> beanKind ) throws ORMException, SQLException, NoSuchMethodException{
        this.setBeanKind(beanKind);
        this.setQueryResult(queryResult);
        return getBeanFrom();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList generateListOfBeans(ResultSet queryResult, Class beanKind) throws NoSuchMethodException{
        ArrayList<Object> listOfBeans = new ArrayList();
        try {
            while (queryResult.next()) {
                    listOfBeans.add(generateBean(queryResult, beanKind));
            }
        }catch (ORMException ex) {
            ex.printStackTrace();
        }catch (SQLException ex) {
        	ex.printStackTrace();
        }
        return listOfBeans;
    }
    
    private Object getBeanFrom() throws ORMException, NoSuchMethodException{
        Field[] listOFields = this.getBeanKind().getDeclaredFields();
        try{
        	Object bean = (Object) beanKind.newInstance();
                for(Field field : listOFields){
                        Object fieldValue = this.getQueryResult().getObject(field.getName());
                        Method method = this.getBeanKind().getMethod(SET_PREFIX + firstLetterToCapital(field.getName()), field.getType());
                        method.invoke(bean, fieldValue);
                 }
            return bean;
        }catch (SecurityException exception) {
            throw new ORMException(exception.getMessage(),exception);
        }catch (IllegalArgumentException exception) {
            System.out.println(exception.toString());
            throw new ORMException(exception.getMessage(), exception);
        } catch (InvocationTargetException exception) {
            throw new ORMException(exception.getMessage(), exception);
        } catch (InstantiationException exception) {
            throw new ORMException(exception.getMessage(), exception);
        } catch (IllegalAccessException exception) {
            throw new ORMException(exception.getMessage(), exception);
        } catch (SQLException exception) {
            throw new ORMException(exception.getMessage(), exception);
        }
    }

    private String firstLetterToCapital(String toConvert){
        return toConvert.substring(0,1).toUpperCase() + toConvert.substring(1);
    }

}
