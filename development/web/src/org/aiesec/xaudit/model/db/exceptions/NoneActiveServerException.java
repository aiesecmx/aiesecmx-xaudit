/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.db.exceptions;

/**
 *
 * @author Romms
 */
public class NoneActiveServerException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message="There is none active data base server, verify "
            + "your configuration file";

    public NoneActiveServerException(){
        super();
    }

    protected String getExceptionMessage(){
        return message;
    }
}
