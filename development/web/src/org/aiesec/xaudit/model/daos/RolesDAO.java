package org.aiesec.xaudit.model.daos;

import org.aiesec.xaudit.model.beans.RoleBean;
import org.aiesec.xaudit.model.db.connectionpool.DbConnection;
import org.aiesec.xaudit.model.db.connectionpool.DbConnectionManager;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

import java.sql.SQLException;
import java.util.ArrayList;

public class RolesDAO extends AbstractDAO<RoleBean> {

	DbConnection dbConnection;
	
	@Override
	public void insert(RoleBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(RoleBean bean, String condition) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ArrayList<RoleBean> select() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        DbConnection dbConnection;
            dbConnection = DbConnectionManager.getInstance().getConnection();
			ArrayList<RoleBean> roleList = beansGenerator.generateListOfBeans
                    (dbConnection.executeQuery(this.getQuery()), RoleBean.class);
            DbConnectionManager.getInstance().closeDbConnection(dbConnection);
            
            return roleList;
	}

	@Override
	public ArrayList<RoleBean> selectAll() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        this.setQuery("SELECT * FROM Roles");
        return this.select();
	}

	@Override
	public ArrayList<RoleBean> selectWhere(String condition)
			throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        this.setQuery("SELECT * FROM Roles WHERE "+condition);
        return this.select();
	}

	@Override
	public void delete(RoleBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

}
