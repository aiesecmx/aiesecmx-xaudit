/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.aiesec.xaudit.model.daos;

/**
 *
 * @author romms
 */
public class DAOException extends Exception{

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DAOException(String message, Exception exception){
            super(message,exception);
        }

        public DAOException(Exception exception){
            super(exception);
        }

        public String getExceptionMessage(){
        return this.getMessage();
        }

}
