package org.aiesec.xaudit.model.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.aiesec.xaudit.model.beans.LanguageBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

public class LanguagesDAO extends AbstractDAO<LanguageBean> {

	@Override
	public void insert(LanguageBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(LanguageBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(LanguageBean bean, String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ArrayList<LanguageBean> select() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<LanguageBean> selectAll() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<LanguageBean> selectWhere(String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
