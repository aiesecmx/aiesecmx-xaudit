package org.aiesec.xaudit.model.daos;

import org.aiesec.xaudit.model.beans.UserBean;
import org.aiesec.xaudit.model.db.connectionpool.DbConnection;
import org.aiesec.xaudit.model.db.connectionpool.DbConnectionManager;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

import java.sql.SQLException;
import java.util.ArrayList;

public class UsersDAO extends AbstractDAO<UserBean>{
	
	DbConnection dbConnection;
	
	@Override
	public void insert(UserBean bean) throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
			dbConnection = DbConnectionManager.getInstance().getConnection();
	        String sqlStatement = "INSERT INTO users(name, lastName, email, "
	        	+ "password, roleId, active)values"
                + "('" + bean.getName()+ "','" 
	        	+ bean.getLastName()+"','"
                + bean.getEmail()+ "','" 
                + bean.getPassword()+"','"
                + bean.getRoleId()+"','"
                + bean.isActive()+ "');";
	        dbConnection.executeUpdate(sqlStatement);
	        DbConnectionManager.getInstance().closeDbConnection(dbConnection);
	}

	@Override
	public void update(UserBean bean, String condition) throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
		this.dbConnection = DbConnectionManager.getInstance().getConnection();
        String sqlStatement = "UPDATE Users  SET " +
        		"name='"+bean.getName()+"',"+
        		"lastName='"+bean.getLastName()+"',"+
        		"email='"+bean.getEmail()+"',"+
        		"password='"+bean.getPassword()+"',"+
        		"roleId='"+bean.getRoleId()+"',"+
        		"active="+bean.isActive()+ 
        		" WHERE "+condition;
        System.out.println(sqlStatement);
        this.dbConnection.executeUpdate(sqlStatement);
        DbConnectionManager.getInstance().closeDbConnection(this.dbConnection);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ArrayList<UserBean> select() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        DbConnection dbConnection;
        dbConnection = DbConnectionManager.getInstance().getConnection();
        System.out.println(this.getQuery());
		ArrayList<UserBean> userList = beansGenerator.generateListOfBeans
                (dbConnection.executeQuery(this.getQuery()), UserBean.class);
        DbConnectionManager.getInstance().closeDbConnection(dbConnection);
        
        return userList;
	}

	@Override
	public ArrayList<UserBean> selectAll() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception {
        this.setQuery("SELECT * FROM Users");
        return this.select();
	}

	@Override
	public ArrayList<UserBean> selectWhere(String condition) throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception{
	    this.setQuery("SELECT * FROM Users WHERE "+condition);
	    return this.select();
	}

	@Override
	public void delete(UserBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		
	}
	
	public void disable(long userId) throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception{
		this.dbConnection = DbConnectionManager.getInstance().getConnection();
        String sqlStatement = "UPDATE Users  SET " +
        		"active=0 WHERE id="+userId;
       System.out.println(sqlStatement);
        this.dbConnection.executeUpdate(sqlStatement);
        DbConnectionManager.getInstance().closeDbConnection(this.dbConnection);
	}

	

}
