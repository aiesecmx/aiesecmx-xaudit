package org.aiesec.xaudit.model.daos;

import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;
import org.aiesec.xaudit.model.db.orm.BeansGenerator;

import java.sql.SQLException;
import java.util.ArrayList;

/**
*
* @author Romms
*/
public abstract class AbstractDAO<T> {

    BeansGenerator beansGenerator = new BeansGenerator();

    private String query;

    protected String getQuery() {
        return query;
    }

    protected void setQuery(String query) {
        this.query = query;
    }

    public abstract void insert(T bean)throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

    public abstract void delete(T bean)throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

    public abstract void update(T bean, String condition)throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

    protected abstract ArrayList<T> select()throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

    public abstract ArrayList<T> selectAll()throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

    public abstract ArrayList<T> selectWhere(String condition)throws DAOException, MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception;

}
