package org.aiesec.xaudit.model.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.aiesec.xaudit.model.beans.StateBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

public class StatesDAO extends AbstractDAO<StateBean> {

	@Override
	public void insert(StateBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(StateBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(StateBean bean, String condition) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ArrayList<StateBean> select() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<StateBean> selectAll() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<StateBean> selectWhere(String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
