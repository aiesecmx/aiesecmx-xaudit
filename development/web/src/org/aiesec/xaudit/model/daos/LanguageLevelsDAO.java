package org.aiesec.xaudit.model.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.aiesec.xaudit.model.beans.LanguageLevelBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

public class LanguageLevelsDAO extends AbstractDAO<LanguageLevelBean>{

	@Override
	public void insert(LanguageLevelBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(LanguageLevelBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(LanguageLevelBean bean, String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ArrayList<LanguageLevelBean> select() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<LanguageLevelBean> selectAll() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<LanguageLevelBean> selectWhere(String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
