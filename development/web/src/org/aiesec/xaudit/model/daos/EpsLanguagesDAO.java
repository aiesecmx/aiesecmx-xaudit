package org.aiesec.xaudit.model.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.aiesec.xaudit.model.beans.EpLanguageBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

public class EpsLanguagesDAO extends AbstractDAO<EpLanguageBean>{

	@Override
	public void insert(EpLanguageBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(EpLanguageBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(EpLanguageBean bean, String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ArrayList<EpLanguageBean> select() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<EpLanguageBean> selectAll() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<EpLanguageBean> selectWhere(String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
