package org.aiesec.xaudit.model.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.aiesec.xaudit.model.beans.CityBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

public class CitiesDAO extends AbstractDAO<CityBean> {

	@Override
	public void insert(CityBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(CityBean bean) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(CityBean bean, String condition) throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ArrayList<CityBean> select() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<CityBean> selectAll() throws DAOException,
			MoreThanOneDbServerActiveException, NoneActiveServerException,
			SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<CityBean> selectWhere(String condition)
			throws DAOException, MoreThanOneDbServerActiveException,
			NoneActiveServerException, SQLException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
