package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class RoleBean implements Serializable{
	
	private static final long serialVersionUID = -5395250810669979095L;
	
	private long id;
	private String name;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
