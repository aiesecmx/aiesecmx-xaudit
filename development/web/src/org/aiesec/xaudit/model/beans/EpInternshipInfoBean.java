package org.aiesec.xaudit.model.beans;

import java.io.Serializable;
import java.sql.Date;

public class EpInternshipInfoBean implements Serializable{

	private static final long serialVersionUID = 4532295211240437398L;
	
	private long id;
	private long epId;
	private long internshipTypeId;
	private long duration;
	private Date erliestStart;
	private Date latestEnd;
	private String reciptPath;
	private long epManagerId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getEpId() {
		return epId;
	}
	public void setEpId(long epId) {
		this.epId = epId;
	}
	public long getInternshipTypeId() {
		return internshipTypeId;
	}
	public void setInternshipTypeId(long internshipTypeId) {
		this.internshipTypeId = internshipTypeId;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public Date getErliestStart() {
		return erliestStart;
	}
	public void setErliestStart(Date erliestStart) {
		this.erliestStart = erliestStart;
	}
	public Date getLatestEnd() {
		return latestEnd;
	}
	public void setLatestEnd(Date latestEnd) {
		this.latestEnd = latestEnd;
	}
	public String getReciptPath() {
		return reciptPath;
	}
	public void setReciptPath(String reciptPath) {
		this.reciptPath = reciptPath;
	}
	public long getEpManagerId() {
		return epManagerId;
	}
	public void setEpManagerId(long epManagerId) {
		this.epManagerId = epManagerId;
	}

}
