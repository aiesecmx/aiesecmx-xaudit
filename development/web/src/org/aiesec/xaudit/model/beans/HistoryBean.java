package org.aiesec.xaudit.model.beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class HistoryBean implements Serializable {
	
	private static final long serialVersionUID = -4433891244506283849L;
	
	private long id;
	private long userId;
	private Timestamp when;
	private String what;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Timestamp getWhen() {
		return when;
	}
	public void setWhen(Timestamp when) {
		this.when = when;
	}
	public String getWhat() {
		return what;
	}
	public void setWhat(String what) {
		this.what = what;
	}

}
