package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class EpManagerBean implements Serializable {

	private static final long serialVersionUID = -1931282396616387649L;
	
	private long id;
	private String phone;
	private long committeeId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public long getCommitteeId() {
		return committeeId;
	}
	public void setCommitteeId(long committeeId) {
		this.committeeId = committeeId;
	}

}
