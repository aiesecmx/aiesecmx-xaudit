package org.aiesec.xaudit.model.beans;

import java.io.Serializable;
import java.sql.Date;

public class EpInfoBean implements Serializable{
	
	private static final long serialVersionUID = -4601483018620834130L;
	
	private long id;
	private Date bday;
	private String phone;
	private String university;
	private String studyArea;
	private Date yearOfCompletion;
	private long committeeId;
	private String passportPath;
	private String cvPath;
	private long userId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getBday() {
		return bday;
	}
	public void setBday(Date bday) {
		this.bday = bday;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getStudyArea() {
		return studyArea;
	}
	public void setStudyArea(String studyArea) {
		this.studyArea = studyArea;
	}
	public Date getYearOfCompletion() {
		return yearOfCompletion;
	}
	public void setYearOfCompletion(Date yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}
	public long getCommitteeId() {
		return committeeId;
	}
	public void setCommitteeId(long committeeId) {
		this.committeeId = committeeId;
	}
	public String getPassportPath() {
		return passportPath;
	}
	public void setPassportPath(String passportPath) {
		this.passportPath = passportPath;
	}
	public String getCvPath() {
		return cvPath;
	}
	public void setCvPath(String cvPath) {
		this.cvPath = cvPath;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

}
