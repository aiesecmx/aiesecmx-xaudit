package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class EpDocStatusBean implements Serializable{
	
	private static final long serialVersionUID = 5007219807193528326L;
	
	private long epId;
	private long reciptStatus;
	private long passportStatus;
	private long cvStatus;
	
	public long getEpId() {
		return epId;
	}
	public void setEpId(long epId) {
		this.epId = epId;
	}
	public long getReciptStatus() {
		return reciptStatus;
	}
	public void setReciptStatus(long reciptStatus) {
		this.reciptStatus = reciptStatus;
	}
	public long getPassportStatus() {
		return passportStatus;
	}
	public void setPassportStatus(long passportStatus) {
		this.passportStatus = passportStatus;
	}
	public long getCvStatus() {
		return cvStatus;
	}
	public void setCvStatus(long cvStatus) {
		this.cvStatus = cvStatus;
	}

}
