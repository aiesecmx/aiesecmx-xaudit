package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class StateBean implements Serializable{
	
	private static final long serialVersionUID = 965335908597891493L;
	
	private long id;
	private String name;
	private long countryId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}	

}
