package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class EpUpdateBean implements Serializable {

	private static final long serialVersionUID = -3345930732652752999L;
	
	private long id;
	private long epId;
	private String update;
	private long epManagerId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getEpId() {
		return epId;
	}
	public void setEpId(long epId) {
		this.epId = epId;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public long getEpManagerId() {
		return epManagerId;
	}
	public void setEpManagerId(long epManagerId) {
		this.epManagerId = epManagerId;
	}

}
