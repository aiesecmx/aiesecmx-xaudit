package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class CityBean implements Serializable{

	private static final long serialVersionUID = -6376130627853533101L;
	
	private long id;
	private String name;
	private long stateId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getStateId() {
		return stateId;
	}
	public void setStateId(long stateId) {
		this.stateId = stateId;
	}
	
}
