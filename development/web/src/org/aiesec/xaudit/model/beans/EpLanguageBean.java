package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class EpLanguageBean implements Serializable{

	private static final long serialVersionUID = 3014867169929481373L;
	
	private long epId;
	private long languageId;
	private long levelId;
	
	public long getEpId() {
		return epId;
	}
	public void setEpId(long epId) {
		this.epId = epId;
	}
	public long getLanguageId() {
		return languageId;
	}
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}
	public long getLevelId() {
		return levelId;
	}
	public void setLevelId(long levelId) {
		this.levelId = levelId;
	}

}
