package org.aiesec.xaudit.model.beans;

import java.io.Serializable;

public class CommitteeBean implements Serializable {
	
	private static final long serialVersionUID = -2840847840871823404L;
	
	private long id;
	private String name;
	private long cityId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

}
