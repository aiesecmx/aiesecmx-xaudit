package org.aiesec.xaudit.services;

import org.aiesec.xaudit.model.beans.UserBean;
import org.aiesec.xaudit.model.daos.UsersDAO;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;

import java.sql.SQLException;
import java.util.ArrayList;

public class UsersServices {
	private UsersDAO userDao;
	
	public UsersServices(){
		this.userDao=new UsersDAO();
	}
	
	
	public ArrayList<UserBean> selectAllActiveUsers() throws MoreThanOneDbServerActiveException, NoneActiveServerException, SQLException, Exception{
		return this.userDao.selectWhere("active=1");
	}
}
