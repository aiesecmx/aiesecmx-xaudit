package org.aiesec.xaudit.actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SessionManagementActions extends ActionSupport{
	
	private static final long serialVersionUID = -1974623597013654336L;
	
	private String userId;
	  private String password;
	  
	  public String login() throws Exception{
		  if ("admin".equals(userId) && "admin".equals(password)) {
			  Map session = ActionContext.getContext().getSession();
			  session.put("logged-in","true");
			  return SUCCESS;
		  }
		  else{
			  return ERROR;
		  }
	  }

	  public String logout() throws Exception {
		  Map session = ActionContext.getContext().getSession();
		  session.remove("logged-in");
		  return SUCCESS;
	  }

	  public String getPassword() {
		  return password;
	  }

	  public void setPassword(String password) {
		  this.password = password;
	  }

	  public String getUserId() {
		  return userId;
	  }

	  public void setUserId(String userId) {
		  this.userId = userId;
	  }
}
