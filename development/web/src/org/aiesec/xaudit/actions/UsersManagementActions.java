package org.aiesec.xaudit.actions;

import org.aiesec.xaudit.model.beans.UserBean;
import org.aiesec.xaudit.model.db.exceptions.MoreThanOneDbServerActiveException;
import org.aiesec.xaudit.model.db.exceptions.NoneActiveServerException;
import org.aiesec.xaudit.services.UsersServices;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class UsersManagementActions extends ActionSupport{
	private static final long serialVersionUID = 1L;
	//--------------Variables
	private static UserBean userBean;

	private List<UserBean> listOfUsers= new ArrayList<UserBean>();

	private UsersServices usersServices=new UsersServices();
	
	private String jsonData;
	//--------------End of Variables

	//--------------Actions	
	public String welcome(){
		return SUCCESS;
	}
	
	public String login(){
		return SUCCESS;
	}
	
	public String showUsersManagement(){
		try {
			this.listOfUsers = this.usersServices.selectAllActiveUsers();
			Gson gson = new Gson();
			jsonData = gson.toJson(listOfUsers);
			return SUCCESS;
        }catch (MoreThanOneDbServerActiveException e){
            e.printStackTrace();
            return ERROR;
	    }catch (NoneActiveServerException e){
	            e.printStackTrace();
	            return ERROR;
	    }catch (SQLException e){
	            e.printStackTrace();
	            return ERROR;
	    }catch (Exception e){
	            e.printStackTrace();
	            return ERROR;
	    }
	}

	//--------------End of Actions

	
	//--------------Getters and setters
	public List<UserBean> getListOfUsers() {
		return listOfUsers;
	}

	public void setListOfUsers(List<UserBean> listOfUsers) {
		this.listOfUsers = listOfUsers;
	}
	
	public UserBean getUserBean() {
		return userBean;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	//--------------End of Getters and setters
}
